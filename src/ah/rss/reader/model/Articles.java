package ah.rss.reader.model;


import java.time.LocalDateTime;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.StringProperty;

/**
 * 
 * Model class for a Articles
 * @author maxime
 *
 */

public class Articles {

	private final StringProperty title;
	private final StringProperty description;
	private final StringProperty author;
	private final StringProperty source;
	private final StringProperty link;
    private final ObjectProperty<LocalDateTime> pubDate;
	private final StringProperty category;
	private final BooleanProperty favorite;
	private final BooleanProperty read;
	private final StringProperty uid;

	/**
	 * Default constructor
	 */
	public Articles() {
		this(null, null, null, null, null, null, null, null);
	}


	/**
	 * Param constructor
	 */
	public Articles(String title, String author, String link, String description, String category, String source, LocalDateTime pubDate, String uid) {
		this.title = new SimpleStringProperty(title);
		this.author= new SimpleStringProperty(author);
		this.link = new SimpleStringProperty(link);
		this.description = new SimpleStringProperty(description);
		this.category = new SimpleStringProperty(category);
		this.source = new SimpleStringProperty(source);
		this.pubDate = new SimpleObjectProperty<LocalDateTime>(pubDate);
		this.read = new SimpleBooleanProperty(false);
		this.favorite = new SimpleBooleanProperty(false);
		this.uid = new SimpleStringProperty(uid);
	}

	
	/**
	 * 
	 * 
	 * Property getters
	 */
	public StringProperty getTitleProperty() {return title;}
	public StringProperty getAuthorProperty() {return author;}
	public StringProperty getLinkProperty() {return link;}
	public StringProperty getDescriptionProperty() {return description;}
	public StringProperty getCategoryProperty() {return category;}
	public StringProperty getSourceProperty() {return source;}
	public ObjectProperty<LocalDateTime> getPubDateProperty() {return pubDate;}
	public BooleanProperty getReadProperty() {return read;}
	public BooleanProperty getFavoriteProperty() {return favorite;}
	public StringProperty getUidProperty() {return uid;}

	/**
	 * 
	 * Typed getters
	 * 
	 */
	public String getTitle() {return title.get();}
	public String getAuthor() {return author.get();}
	public String getLink() {return link.get();}
	public String getDescription() {return description.get();}
	public String getCategory() {return category.get();}
	public String getSource() {return source.get();}
	public LocalDateTime getPubDate() {return pubDate.get();}
	public Boolean getRead() {return read.get();}
	public Boolean getFavorite() {return favorite.get();}
	public String getUid() {return uid.get();}
	
	/**
	 * 
	 * Setters
	 */
	public void setTitle(String title) {this.title.set(title);}
	public void setAuthor(String author) {this.author.set(author);}
	public void setLink(String link) {this.link.set(link);}
	public void setDescription(String description) {this.description.set(description);}
	public void setCategory(String category) {this.category.set(category);}
	public void setSource(String source) {this.source.set(source);}
	public void setPubDate(LocalDateTime pubDate) {this.pubDate.set(pubDate);}
	public void setRead(boolean read) {this.read.set(!read);}
	public void setFavorite(boolean favorite) {this.favorite.set(favorite);}
	public void setUid(String uid) {this.uid.set(uid);}

	/**
	 * 
	 * methods
	 */
	public void toggleFavorite(boolean favorite) {
		this.favorite.set(!favorite);
	}



}
