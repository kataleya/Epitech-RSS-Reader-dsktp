package ah.rss.reader.model;

import java.time.LocalDateTime;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.IntegerProperty;

/**
 * 
 * Model class for a Feeds
 * @author maxime
 *
 */
public class Feeds {

	private final StringProperty uid;
	private final StringProperty source;
	private final StringProperty title;
	private final StringProperty link;
	private final StringProperty description;
	private final IntegerProperty currentOffset;
	private final StringProperty language;
	private final StringProperty copyright;
	private final StringProperty imageUrl;
	private final ObjectProperty<LocalDateTime> lastBuildDate;
	private final StringProperty ttl;
	private final StringProperty recvUnixDate;
	
	/**
	 * Default constructor
	 */
	public Feeds() {
		this(null, null, null, null, null, null, null, null, null, null, null, null);
	}

	/**
	 * URLconstructor
	 */
	public Feeds(String url) {
		this.title = new SimpleStringProperty("");
		this.description = new SimpleStringProperty("");
		this.imageUrl = new SimpleStringProperty("");
		this.link = new SimpleStringProperty("");
		this.source = new SimpleStringProperty(url);
		this.language = new SimpleStringProperty("");
		this.copyright = new SimpleStringProperty("");
		this.lastBuildDate = new SimpleObjectProperty<LocalDateTime>(LocalDateTime.now());
		this.uid = new SimpleStringProperty("");
		this.currentOffset = new SimpleIntegerProperty(0);
		this.ttl = new SimpleStringProperty("");
		this.recvUnixDate = new SimpleStringProperty("");
	}


	/**
	 * Param constructor
	 */
	public Feeds(String title, String description, String imageUrl, String link, String source, String language, String copyright, LocalDateTime lastBuildDate, String uid, Integer currentOffset, String ttl, String recvUnixDate) {
		this.title = new SimpleStringProperty(title);
		this.description = new SimpleStringProperty(description);
		this.imageUrl = new SimpleStringProperty(imageUrl);
		this.link = new SimpleStringProperty(link);
		this.source = new SimpleStringProperty(source);
		this.language = new SimpleStringProperty(language);
		this.copyright = new SimpleStringProperty(copyright);
		this.lastBuildDate = new SimpleObjectProperty<LocalDateTime>(lastBuildDate);
		this.uid = new SimpleStringProperty(uid);
		this.currentOffset = new SimpleIntegerProperty(currentOffset);
		this.ttl = new SimpleStringProperty(ttl);
		this.recvUnixDate = new SimpleStringProperty(recvUnixDate);
	}

	
	/**
	 * 
	 * 
	 * Property getters
	 */
	public IntegerProperty getCurrentOffsetProperty() {return currentOffset;}
	public StringProperty getTitleProperty() {return title;}
	public StringProperty getDescriptionProperty() {return description;}
	public StringProperty getImageUrlProperty() {return imageUrl;}
	public StringProperty getSourceProperty() {return source;}
	public StringProperty getLinkProperty() {return link;}
	public StringProperty getLanguageProperty() {return language;}
	public StringProperty getUidProperty() {return uid;}
	public ObjectProperty<LocalDateTime> getLastBuildDateProperty() {return lastBuildDate;}
	public StringProperty getCopyrightProperty() {return copyright;}
	public StringProperty getTTLProperty() {return ttl;}
	public StringProperty getRecvUnixDateProperty() {return recvUnixDate;}


	/**
	 * 
	 * Typed getters
	 * 
	 */
	public int getCurrentOffset() {return currentOffset.get();}
	public String getTitle() {return title.get();}
	public String getDescription() {return description.get();}
	public String getImageUrl() {return imageUrl.get();}
	public String getSource() {return source.get();}
	public String getLink() {return link.get();}
	public String getLanguage() {return language.get();}
	public String getUid() {return uid.get();}
	public LocalDateTime getLastBuildDate() {return lastBuildDate.get();}
	public String getCopyright() {return copyright.get();}
	public String getTTL() {return ttl.get();}
	public String getRecvUnix() {return recvUnixDate.get();}
	
	/**
	 * 
	 * Setters
	 */
	public void setCurrentOffset(int offset) {this.currentOffset.set(offset);}
	public void setTitle(String title) {this.title.set(title);}
	public void setDescription(String description) {this.description.set(description);}
	public void setImageUrl(String imageUrl) {this.imageUrl.set(imageUrl);}
	public void setSource(String source) {this.source.set(source);}
	public void setLink(String link) {this.link.set(link);}
	public void setLanguage(String language) {this.language.set(language);}
	public void setUid(String uid) {this.uid.set(uid);}
	public void setLastBuildDate(LocalDateTime lastBuildDate) {this.lastBuildDate.set(lastBuildDate);}
	public void setCopyright(String copyright) {this.copyright.set(copyright);}
	public void setTTL(String ttl) {this.ttl.set(ttl);}
	public void setRecvUnix(String recvUnix) {this.recvUnixDate.set(recvUnix);}
}
