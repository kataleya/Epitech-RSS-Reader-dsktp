//package ah.rss.reader.model;
//
//import java.util.List;
//
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlRootElement;
//
///**
// * Helper class to wrap a list of Feeds. This is used for saving the
// * list of feeds to XML.
// * 
// * @author Max
// */
//@XmlRootElement(name = "feeds")
//public class FeedsWrapper {
//	private List<Feeds> feeds;
//
//    @XmlElement(name = "feeds")
//    public List<Feeds> getFeeds() {
//        return feeds;
//    }
//
//    public void setFeeds(List<Feeds> feeds) {
//        this.feeds = feeds;
//    }
//}
