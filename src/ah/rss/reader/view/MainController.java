package ah.rss.reader.view;

import ah.rss.reader.MainApp;
import ah.rss.reader.model.Feeds;
import javafx.fxml.FXML;
import javafx.scene.control.Menu;


public class MainController {
	@FXML
	private Menu FileMenu;
	
    private MainApp mainApp;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public MainController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    @FXML
    private void handleLogin() throws Exception {
    	System.out.println("This should trigger login");
    	mainApp.loginAction();
    }

    @FXML
    private void handleClose() throws Exception {
    	System.out.println("This should trigger close");
    	mainApp.stop();
    }
    /**
     * Called when the user clicks the new feed button. Opens a dialog to add
     * url for a new feed.
     */
    @FXML
    private void handleNewFeed() {
        Feeds feed = new Feeds("");
        boolean okClicked = mainApp.showAddFeedDialog(feed);
        if (okClicked) {
            mainApp.getFeeds().add(feed);
        }
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
}
