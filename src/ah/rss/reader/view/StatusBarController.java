package ah.rss.reader.view;

import ah.rss.reader.MainApp;
import javafx.fxml.FXML;
import javafx.scene.control.Menu;

public class StatusBarController {
	@FXML
	private Menu FileMenu;
	
    private MainApp mainApp;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public StatusBarController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
//		System.out.println(mainApp.getArticles().toString());
//		articlesTable.setItems(mainApp.getArticles());
    }
}
