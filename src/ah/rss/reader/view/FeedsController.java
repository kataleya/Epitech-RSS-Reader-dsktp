package ah.rss.reader.view;

import ah.rss.reader.MainApp;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import ah.rss.reader.model.Feeds;

public class FeedsController {

	@FXML
	private Button addButton;
	@FXML
	private Button rmButton;
	@FXML
	private ListView<Feeds> feedsListView;
	
    private MainApp mainApp;
    private Feeds activeFeed;
    protected ListProperty<Feeds> listProperty = new SimpleListProperty<>();
    
    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public FeedsController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    	feedsListView.setCellFactory(param -> new ListCell<Feeds>() {
    	    @Override
    	    protected void updateItem(Feeds item, boolean empty) {
    	        super.updateItem(item, empty);
    	        if (empty || item == null || item.getTitle() == null) {
    	            setText(null);
    	        } else {
    	            setText(item.getTitle());
    	        }
    	    }
    	});
    	feedsListView.getSelectionModel().selectedItemProperty().addListener(
    			(observable, oldValue, newValue) -> setActiveFeed(newValue));
    }
    
    /**
     * Called when the user clicks the new feed button. Opens a dialog to add
     * url for a new feed.
     */
    @FXML
    private void handleNewFeed() {
        Feeds feed = new Feeds("");
        boolean okClicked = mainApp.showAddFeedDialog(feed);
        if (okClicked) {
        	System.out.println("ok clicked  " + feed.toString());
        	mainApp.setFeed(feed);
        }
    }

    /**
     * Called when the user clicks the new feed button. Opens a dialog to add
     * url for a new feed.
     */
    @FXML
    private void handleRmFeed() {
        mainApp.getFeeds().remove(activeFeed);
    }

    private void setActiveFeed(Feeds feed) {
//    	System.out.println("should set feed in mainapp " + feed.toString());
		mainApp.setSelectedFeed(feed);
		activeFeed = feed;
    }
    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
		feedsListView.setItems(mainApp.getFeeds());
    }
}
