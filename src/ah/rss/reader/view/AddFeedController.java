package ah.rss.reader.view;

import ah.rss.reader.MainApp;
import ah.rss.reader.model.Feeds;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AddFeedController {
	@FXML
	private TextField URLInput;
	
    private MainApp mainApp;
    private Stage dialogStage;
    private boolean okClicked = false;
    private Feeds feed;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public AddFeedController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
    
    /**
     * Sets the person to be edited in the dialog.
     *
     * @param person
     */
    public void setFeed(Feeds feed) {
        this.feed = feed;
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }
    
    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
        	System.out.println("input valid " + URLInput.getText());
            feed.setSource(URLInput.getText());
            okClicked = true;
            dialogStage.close();
        } else {
        	System.out.println("NOT VALID " + URLInput.getText());
        }
    }
    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }
    
    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

//        TODO Validate the url issues to a real RSS feed
        if (URLInput.getText() == null || URLInput.getText().length() == 0) {
            errorMessage += "No valid URL!\n";
        }
       if (errorMessage.length() == 0) {
        return true;
       } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);
            alert.showAndWait();

            return false;
        }
    }
}

