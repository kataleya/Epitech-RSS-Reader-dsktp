package ah.rss.reader.view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import ah.rss.reader.MainApp;
import ah.rss.reader.model.Articles;

public class ArticlesOverviewController {
	@FXML
	private TableView<Articles> articlesTable;
	@FXML
	private TableColumn<Articles, String> titleColumn;
	@FXML
	private TableColumn<Articles, String> authorColumn;
	@FXML
	private TableColumn<Articles, String> linkColumn;
	@FXML
	private TableColumn<Articles, LocalDateTime> publishedColumn;
	@FXML
	private TableColumn<Articles, Boolean> readColumn;
	@FXML
	private TableColumn<Articles, Boolean> favoriteColumn;

	@FXML
	private Label titleLabel;
	@FXML
	private Label authorLabel;
	@FXML
	private Label linkLabel;
	@FXML
	private Label publishedLabel;
	@FXML
	private Label readLabel;
	@FXML
	private Label favoriteLabel;
	@FXML
	private SplitPane articlesPane;
	@FXML
	private TextArea articleText;
	@FXML
	private Button markReadButton;
	@FXML
	private Button favButton;
	
	// Reference to the main application.
    private MainApp mainApp;
    private Articles currentArticle;

    /**
     * The constructor.
     * The constructor is called before the initialize() method.
     */
    public ArticlesOverviewController() {
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // Initialize the articles table with two columns.
        titleColumn.setCellValueFactory(cellData -> cellData.getValue().getTitleProperty());
        authorColumn.setCellValueFactory(cellData -> cellData.getValue().getAuthorProperty());
        linkColumn.setCellValueFactory(cellData -> cellData.getValue().getSourceProperty());
        publishedColumn.setCellValueFactory(cellData -> cellData.getValue().getPubDateProperty());
        favoriteColumn.setCellValueFactory(cellData -> cellData.getValue().getFavoriteProperty());
        readColumn.setCellValueFactory(cellData -> cellData.getValue().getReadProperty());
        showArticleDetails(null);
        articlesTable.getSelectionModel().selectedItemProperty().addListener(
        		(observable, oldValue, newValue) -> showArticleDetails(newValue));
    }

    /**
     * Is called by the main application to give a reference back to itself.
     * 
     * @param mainApp
     */
    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
		articlesTable.setItems(mainApp.getArticles());
    }
    
    public void setRead() {
    	System.out.println("This article should be marked as read " + currentArticle);
    }
    public void setFavorite() {
    	System.out.println("This article should be marked as favorite " + currentArticle);
    }

    private void showArticleDetails(Articles article) {
    	if (article != null) {
    		currentArticle = article;
    		String tmpTitle = (article.getTitle() == null ) ? article.getDescription() : article.getTitle();
    		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    		titleLabel.setText(tmpTitle);
    		authorLabel.setText(article.getAuthor());
    		publishedLabel.setText(article.getPubDate().format(formatter));
    		articleText.setText(article.getDescription());
    	} else {
      		titleLabel.setText("");
    		authorLabel.setText("");
    		publishedLabel.setText("");
    		articleText.setText("");
    	}
    }
}
