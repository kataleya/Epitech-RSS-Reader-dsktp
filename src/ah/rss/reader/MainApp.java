package ah.rss.reader;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.time.LocalDateTime;

import ah.rss.reader.model.Articles;
import ah.rss.reader.model.Feeds;
import ah.rss.reader.view.ArticlesOverviewController;
import ah.rss.reader.view.LoginController;
import ah.rss.reader.view.FeedsController;
import ah.rss.reader.view.AddFeedController;
import ah.rss.reader.view.MainController;
import ah.rss.reader.view.StatusBarController;
import ah.rss.reader.HttpClient;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainApp extends Application {

	private Stage primaryStage;
	private BorderPane rootLayout;
	private ObservableList<Articles> articlesData = FXCollections.observableArrayList();
	private ObservableList<Feeds> feedsData = FXCollections.observableArrayList();
	private MainController mainController;
	private ArticlesOverviewController articlesOverviewController; 
	private FeedsController feedsController;
	private StatusBarController statusBarController;
	
	private VBox articlesPane;
	private VBox feedsPane;
	private HBox statusBar;
	private String validUserToken;

	private Feeds selectedFeed;
	
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("AH RSS reader");
	    this.primaryStage.setOnCloseRequest(e -> Platform.exit());
		try {
			initRootLayout();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void stop() throws Exception {
		super.stop();
		System.out.println("Clean exiting");
	}

	public MainApp() {
	  articlesData.add(new Articles("First article", "Mc ct","www.qwant.com", "Lorem ipsum indolore et super cool 180 tailwhip barspin wazapfiq qsd qsda",
			  "trash category", "www.mooncul.com", LocalDateTime.now(), "hash1" ));
	  articlesData.add(new Articles("Second article","Mc ct","www.qwant.com","Total Lorem ipsum indolore et super cool 180 tailwhip barspin wazapfiq qsd qsda",
			  "trash category","www.mooncul.com", LocalDateTime.now(), "hash1"));
	  articlesData.add(new Articles("First article", "Mc ct","www.qwant.com", "Lorem ipsum indolore et super cool 180 tailwhip barspin wazapfiq qsd qsda",
			  "trash category", "www.mooncul.com", LocalDateTime.now(), "hash2" ));
	  articlesData.add(new Articles("Second article","Mc ct","www.qwant.com","Total Lorem ipsum indolore et super cool 180 tailwhip barspin wazapfiq qsd qsda",
			  "trash category","www.mooncul.com", LocalDateTime.now(), "hash2"));
	  feedsData.add(new Feeds("feed1Title","feedDescription","feedImgUrl.com","feedLink.com","feedSource.com","feedLanguage","feedCopyright",LocalDateTime.now(),"thisshouldbeahashqs32d1a6dqs3dasaa",20,"",""));
	  feedsData.add(new Feeds("feed2Title","feed2Description","feed2ImgUrl.com","feed32Link.com","feed2Source.com","feed2Language","feedCopyright",LocalDateTime.now(),"thisshouldbeahashqs32d1a6dqs3daaas",20,"",""));
	}
	
	  public void initRootLayout() throws IOException {
	    FXMLLoader loader = new FXMLLoader();
		try {
			loader.setLocation(MainApp.class.getResource("view/ReaderView.fxml"));
		    rootLayout = (BorderPane) loader.load();
			Scene scene = new Scene(rootLayout);

			// Show the scene containing the root layout.
			primaryStage.setScene(scene);
			primaryStage.show();

			// builds global splitPane by first adding feeds view then articles view
			SplitPane mainSplit = new SplitPane();
			
			// Loader for articles layout
			loadArticlesOverview();
			loadFeedsOverview();
			loadStatusBarView();
			mainSplit.getItems().add(feedsPane);
			mainSplit.getItems().add(articlesPane);
		    rootLayout.setCenter(mainSplit);
		    rootLayout.setBottom(statusBar);
						
			
			//Give the controller access to the main app
			mainController = loader.getController();
			mainController.setMainApp(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	  }

	  /**
	     * Shows the article overview inside the root layout.
	     */
	    public void loadArticlesOverview() {
	        try {
	            // Load articles overview.
	            FXMLLoader loader = new FXMLLoader();
	            loader.setLocation(MainApp.class.getResource("view/ArticlesOverview.fxml"));
			    articlesPane = (VBox) loader.load();
				articlesOverviewController = loader.getController();
				articlesOverviewController.setMainApp(this);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }

	    public void loadFeedsOverview() {
	    	try {
	    		// Load feeds overview.
	            FXMLLoader loader = new FXMLLoader();
	            loader.setLocation(MainApp.class.getResource("view/FeedsView.fxml"));
			    feedsPane = (VBox) loader.load();
				feedsController = loader.getController();
				feedsController.setMainApp(this);
	    	} catch (IOException e) {
	    		e.printStackTrace();
	    	}
	    }

	    public void loadStatusBarView() {
	    	try {
	    		// Load feeds overview.
	            FXMLLoader loader = new FXMLLoader();
	            loader.setLocation(MainApp.class.getResource("view/StatusBarView.fxml"));
			    statusBar = (HBox) loader.load();
				statusBarController = loader.getController();
				statusBarController.setMainApp(this);
	    	} catch (IOException e) {
	    		e.printStackTrace();
	    	}
	    }
	    
	    public void setSelectedFeed(Feeds feed) {
	    	selectedFeed = feed;
	    	System.out.println("Setting selectedFeed " +selectedFeed);
			articlesOverviewController.setMainApp(this);
	    }
		  public boolean loginAction() {
			  if (isValid(validUserToken)) {
				  System.out.println("user token is valid");
				 return true;
			  } else {
				  // show user login dialog
		    	try {
					  System.out.println("waiting for user token validation");
		            FXMLLoader loader = new FXMLLoader();
		            loader.setLocation(MainApp.class.getResource("view/LoginView.fxml"));
				    AnchorPane dialog = (AnchorPane) loader.load();
				    Stage dialogStage = new Stage();
				    dialogStage.setTitle("Login");
				    dialogStage.initModality(Modality.WINDOW_MODAL);
				    dialogStage.initOwner(primaryStage);
				    Scene scene = new Scene(dialog);
				    
				    LoginController controller = loader.getController();
				    System.out.println(controller);
			        controller.setDialogStage(dialogStage);
				    System.out.println(controller);
			        dialogStage.setScene(scene);
//					    controller.setUser(user);
				    dialogStage.showAndWait();

			        return controller.isOkClicked();
		    	} catch(IOException e) {
		    		e.printStackTrace();
		    		return false;
		    	}	    	
			  }
		  }
		  
		  private boolean isValid(String token) {
//			  TODO Ack token here
			  return false;
		  }

	    public boolean showAddFeedDialog(Feeds feed) {
	    	try {
	            FXMLLoader loader = new FXMLLoader();
	            loader.setLocation(MainApp.class.getResource("view/AddFeedView.fxml"));
			    AnchorPane dialog = (AnchorPane) loader.load();
			    Stage dialogStage = new Stage();
			    dialogStage.setTitle("Add feed");
			    dialogStage.initModality(Modality.WINDOW_MODAL);
			    dialogStage.initOwner(primaryStage);
			    Scene scene = new Scene(dialog);
			    AddFeedController controller = loader.getController();
		        controller.setDialogStage(dialogStage);
		        dialogStage.setScene(scene);
			    controller.setFeed(feed);
			    dialogStage.showAndWait();

		        return controller.isOkClicked();
	    	} catch(IOException e) {
	    		e.printStackTrace();
	    		return false;
	    	}	    	
	    }

	  public ObservableList<Articles> getArticles() {
		  if (selectedFeed != null) {
			  System.out.println("Feed selected => "+selectedFeed.toString() + " should display feed's articles");
			// get articles for the selected feed
			  return articlesData;
		  } else {
			  System.out.println("selectedFeed is null, no articles to display");
			  return null;
		  }
	  }
	  public ObservableList<Feeds> getFeeds() {
//		  if (validUserToken != null) {
			// get articles for the selected feed
		return feedsData;
	  }
	  public void setFeed(Feeds feed) {
//		  if (validUserToken != null) {
			// get articles for the selected feed
		this.feedsData.add(feed);
	  }

	  public Stage getPrimaryStage() {
        return primaryStage;
	  }
	
	  public static void main(String[] args) {
		launch(args);
	  }
}

